# NLP

## Mandatory

### [Introduction to Machine Learning - Lectures 1.1 - 2.4](https://www.youtube.com/playlist?list=PLLssT5z_DsK-h9vYZkQkYNWcItqhlRJLN)

### [Natural language processing: An introduction](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3168328/)

### [Spacy 101 Documentation](https://spacy.io/usage/spacy-101)

## Optional

### [Custom Named Entity Recognition with Spacy in Python](https://www.youtube.com/watch?v=UxzyD6gVlC8)

### [Spacy Linguistic Features](https://spacy.io/usage/linguistic-features)

### [A Survey on Recent Advances in Named Entity Recognition from Deep Learning models](https://www.aclweb.org/anthology/C18-1182)

